using Godot;
using System;

public partial class Environment : Node3D
{
	public static float DefaultTrainSpeed = 10;
	public static float TrainSpeed = 10;

	// Exported (non-static) variable for use in editor
	[Export] public float ExportedTrainSpeed
	{
		get => TrainSpeed;
		set => TrainSpeed = value;
	}
}
