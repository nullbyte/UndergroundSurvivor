using Godot;
using System;

public partial class TreeMeshRandomizer : Node3D
{
	[Export] PackedScene[]? TreeScenes;
	[Export] int SpacingVariation = 10;

	public override void _Ready() {
		// Generate random tree mesh and add it as a child
		var rand = new Random();
		var treeMesh = TreeScenes![rand.Next() % TreeScenes.Length].Instantiate();
		AddChild(treeMesh);

		// Offset its position by a random amount
		int offsetX = (rand.Next() % SpacingVariation) - (SpacingVariation / 2);
		int offsetZ = (rand.Next() % SpacingVariation) - (SpacingVariation / 2);
		Position = new Vector3(offsetX, 0, offsetZ);
	}
}
