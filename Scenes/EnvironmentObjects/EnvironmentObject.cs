using Godot;
using System;

public partial class EnvironmentObject: Node3D {
	int MaxDistance = 120;
	
	public override void _PhysicsProcess(double delta) {
		float vel = (float)delta * Environment.TrainSpeed;
		Position += new Vector3(vel, 0, 0);

		// Delete when out of view of the train. May need to extend later
		if (Position.X >= MaxDistance) {
			QueueFree();
		}
	}
}
