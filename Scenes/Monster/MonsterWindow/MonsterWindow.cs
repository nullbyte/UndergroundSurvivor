using Godot;
using System;

public partial class MonsterWindow: Area3D {
	public override void _Ready()
	{
		GetNode<AnimationPlayer>("Animation").Play("Appear");		
	}
	
	public void Seen()
	{
		GetNode<AnimationPlayer>("Animation").Play("Seen");
	}
}
