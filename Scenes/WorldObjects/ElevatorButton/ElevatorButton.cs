using Godot;
using System.Threading.Tasks;

public partial class ElevatorButton : Area3D, IInteractable
{
	bool HasActivated = false;

	public async void Activated(Player player)
	{
		if (HasActivated) return;
		HasActivated = true;

		CameraShake.Intensity = 1;
		await Task.Delay(1000);
		GetNode<AnimationPlayer>("/root/Game/UILayer/BlackOverlay/Animation").Play("FadeToBlack");
		await ToSignal(GetNode<AnimationPlayer>("/root/Game/UILayer/BlackOverlay/Animation"), "animation_finished");
		GetTree().ChangeSceneToFile("res://Scenes/Excuse/Excuse.tscn");
	}
}
