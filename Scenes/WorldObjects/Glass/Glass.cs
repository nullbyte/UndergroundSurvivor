using Godot;
using System;

public partial class Glass : MeshInstance3D
{
	[Export] Mesh? Broken;
	GpuParticles3D? Particles;

	public override void _Ready()
	{
		Particles = GetNode<GpuParticles3D>("Particles");
	}

	public void Break()
	{
		Mesh = Broken;
		Particles!.Emitting = true;
	}
}
