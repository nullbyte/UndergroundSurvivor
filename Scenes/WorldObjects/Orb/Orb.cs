using Godot;
using System;

public partial class Orb : Node3D, IInteractable
{
	public override void _Ready()
	{
		GetNode<AnimationPlayer>("Animation").Play("Bob");
	}

	public void Activated(Player player) {
		GetTree().ChangeSceneToFile("res://Scenes/Title/Title.tscn");
	}

	void OnEnter(Node3D body)
	{
		if (body is Player)
		{
			GetTree().ChangeSceneToFile("res://Scenes/Title/Title.tscn");
		}
	}
}
