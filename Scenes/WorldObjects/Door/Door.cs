using Godot;
using System;

public partial class Door: AnimatableBody3D
{
	void OpenDoor(Node3D body)
	{
		if (body is Player)
		{
			GetNode<AnimationPlayer>("AnimationPlayer").Play("Open");
			GetNode<Game>("/root/Game").EmitSignal("DoorActivated");
		}
	}

	void CloseDoor(Node3D body)
	{
		if (body is Player)
		{
			GetNode<AnimationPlayer>("AnimationPlayer").PlayBackwards("Open");
		}
	}
}
