using Godot;
using System;

public partial class ChargingStation: Area3D, IInteractable
{
	Game? Game;
	MeshInstance3D? Mesh;

	public override void _Ready()
	{
		Mesh = GetNode<MeshInstance3D>("Mesh");
	}
	
	bool active = false;
	Color activeColor = new Color(1,0,0);
	Color defaultColor = new Color(0,0,1);
	
	public bool Active
	{
		get => active;
		set
		{
			if (value)
			{
				Mesh!.MaterialOverride.Set("albedo_color", activeColor);
				Mesh!.MaterialOverride.Set("emission_enabled", true);
			}
			else
			{
				Mesh!.MaterialOverride.Set("albedo_color", defaultColor);
				Mesh!.MaterialOverride.Set("emission_enabled", false);
			}
			
			active = value;

			Game = GetNode<Game>("/root/Game");
			Game.EmitSignal("ChargerActivated");
		}
	}

	public void Activated(Player player)
	{
		if (!player.FlashLight!.Charging && !Active)
		{
			Active = true;
			player.FlashLight.Charging = true;
		}
		else if (player.FlashLight.Charging && Active)
		{
			Active = false;
			player.FlashLight.Charging = false;
			player.FlashLight.UpdateUI();
			Game!.CompleteHint("Your flashlight is already charging at another station!");
		}
		else if (!player.FlashLight.Charging && !Active)
		{
			_=Game!.ShowHint("Your flashlight is already charging at another station!");
		}
		player.EmitSignal("FlashLightChargeLevelChanged");
	}
}
