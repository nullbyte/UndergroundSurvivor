using Godot;
using System;

public partial class HintArea : Area3D
{
	void OnEnter(Node3D body)
	{
		if (body is Player)
			GetNodeOrNull<HintProvider>("/root/Game/UILayer/UIOffset/HintDisplay/HintProvider")?.OnCrouchPrompt(true);
	}
	void OnExit(Node3D body)
	{
		if (body is Player)
			GetNodeOrNull<HintProvider>("/root/Game/UILayer/UIOffset/HintDisplay/HintProvider")?.OnCrouchPrompt(false);
	}
}
