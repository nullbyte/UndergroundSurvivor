using Godot;
using System.IO;

public partial class Excuse : Control
{
	[Export(PropertyHint.File)] string? ScriptPath;
	// Called when the node enters the scene tree for the first time.
	public async override void _Ready()
	{
		Label label = GetNode<Label>("Label");
		AnimationPlayer animation = GetNode<AnimationPlayer>("Label/Animation");

		string[] scriptData = FileHelper.ReadFile(ScriptPath!);
		
		int i = 0;
		foreach (var line in scriptData) {
			i++;

			// Split each line of script into an array of [time to display, text to display]
			string[] lineData = line.Split(':');

			label.Text = lineData[1];
			
			animation.Play("FadeIn");
			await ToSignal(animation, "animation_finished");

			int delay = int.Parse(lineData[0]);
			await ToSignal(GetTree().CreateTimer(delay), "timeout");

			// Don't fade out after the last line
			if (i < scriptData.Length) {
				animation.Play("FadeOut");
				await ToSignal(animation, "animation_finished");
			}
		}

		GetTree().ChangeSceneToFile("res://Scenes/EndRoom/EndRoom.tscn");
	}
}
