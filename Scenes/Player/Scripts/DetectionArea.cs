using Godot;
using System;
using System.Collections.Generic;

public partial class DetectionArea: Area3D {
	PlayerSanity? Sanity;
	Vignette? Vignette;
	FlashLight? FlashLight;

	List<Area3D> CollidingAreas = new();

	public override void _Ready() {
		FlashLight = GetParent<FlashLight>();
		Vignette = GetNode<Vignette>("%Vignette");
		Sanity = GetNode<PlayerSanity>("%PlayerSanity");
	}
	
	void OnAreaEntered(Area3D area) {
		CollidingAreas.Add(area);
		UpdateVisionCone();
	}

	void OnAreaExited(Area3D area) {
		CollidingAreas.Remove(area);
		UpdateVisionCone();
	}

	public void UpdateVisionCone() {
		bool beingHaunted = false;

		foreach (var area in CollidingAreas) {
			if (area is MonsterWindow monster) {
				if (FlashLight!.IsActive()) {
					monster.Seen();
				} else {
					Vignette!.Darken();
					Sanity!.State = PlayerSanity.SanityState.DRAINING;
					beingHaunted = true;
				}
			}
		}
		if (!beingHaunted) {
			Vignette!.Lighten();
			Sanity!.State = PlayerSanity.SanityState.NORMAL;
		}
	}
}
