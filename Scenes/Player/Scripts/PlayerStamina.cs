using Godot;
using System;

public partial class PlayerStamina: Node {
	float stamina = 100;
	[Export] public float Stamina {
		get => stamina;
		set {
			stamina = Mathf.Clamp(value, 0, 100);
			if (Stamina <= 0)
				State = StaminaState.FULLRECOVERY;
			else if (Stamina >= 100 && State != StaminaState.DRAINING)
				State = StaminaState.DEFAULT;
			UpdateUI();
		}
	}

	[Export] float DrainSpeed = 25;
	[Export] float RecoverSpeed = 15;
	public StaminaState State;
	public enum StaminaState {
		DEFAULT,
		DRAINING,
		RECOVERING,
		FULLRECOVERY
	}

	Player? Player;

	public override void _Ready() {
		Player = GetParent<Player>();
	}
	
	public override void _PhysicsProcess(double delta) {
		if (State == StaminaState.DRAINING)
			Stamina -= DrainSpeed * (float)delta;
		else if (State == StaminaState.RECOVERING || State == StaminaState.FULLRECOVERY)
			Stamina += RecoverSpeed * (float)delta;
	}

	void UpdateUI() {
		Player!.UI?.StaminaBar!.SetValue(Stamina);
	}
}
