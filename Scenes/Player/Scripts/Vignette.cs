using Godot;
using System;

public partial class Vignette: ColorRect {
	public bool Active = false;
	Tween? Animation;
	PlayerSanity? PlayerSanity;

	public override void _Ready()
	{
		PlayerSanity = GetNode<PlayerSanity>("../../PlayerSanity");
		Material.Set("shader_parameter/INTENSITY", 0.5f);
	}

	public void Darken()
	{
		if (!PlayerSanity!.Insane && !Active)
		{
			Active = true;
			Animation = CreateTween();
			Animation.TweenProperty(Material, "shader_parameter/INTENSITY", 1.5f, 0.3f);
		}
	}

	public void Lighten()
	{
		if (!PlayerSanity!.Insane && Active)
		{
			Active = false;
			Animation = CreateTween();
			Animation.TweenProperty(Material, "shader_parameter/INTENSITY", 0.5f, 0.3f);
		}
	}
}
