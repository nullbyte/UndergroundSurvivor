using Godot;
using System;
using System.Threading.Tasks;

public partial class PlayerSanity: Node {
	public bool Insane = false;
	float sanity = 100;
	[Export] public float Sanity
	{
		get => sanity;
		set
		{
			if (!Insane)
			{
				if (value < sanity && !ShownHint)
				{
					ShownHint = true;
					_ = Game!.ShowHint("Looking at monsters will make you go insane, stand still with your light on to calm yourself.");
				}
				else if (value > sanity && ShownHint)
				{
					Game!.CompleteHint("Looking at monsters will make you go insane, stand still with your light on to calm yourself.");
				}

				sanity = Mathf.Clamp(value, 0, 100);
				
				if (value <= 0)
					GoInsane();
				
				UpdateUI();
			}
		}
	}

	bool ShownHint = false;

	// Controls the speed of losing and gaining sanity
	[Export] float DrainSpeed = 10;
	[Export] float RecoverSpeed = 1;
	
	public SanityState State;
	
	public enum SanityState
	{
		NORMAL,
		DRAINING,
		RECOVERING
	}

	// References
	Player? Player;
	Game? Game;
	Vignette? Vignette;

	public override void _Ready()
	{
		Player = GetParent<Player>();
		Game = GetNode<Game>("/root/Game");
		Vignette = GetNode<Vignette>("../ViewLayer/Vignette");
	}
	
	// Drain/gain sanity
	public override void _PhysicsProcess(double delta)
	{
		if (Input.IsActionPressed("MoveCrouch")) return;

		bool sanityChanged = false;

		if (State == SanityState.DRAINING)
		{
			Sanity -= DrainSpeed * (float)delta;
			sanityChanged = true;
		}
		else if (State == SanityState.RECOVERING)
		{
			Sanity += RecoverSpeed * (float)delta;
			sanityChanged = true;
		}

		if (sanityChanged)
			CameraShake.Intensity = (100 - Sanity) / 100;
	}

	void UpdateUI() => Player!.UI?.SanityBar!.SetValue(Sanity);

	// Make the player go insane, then kill them
	async void GoInsane()
	{
		Insane = true;

		Game!.ShowMessage("The darkness takes hold");

		// Darken the players screen
		var fadeTween = GetTree().CreateTween();
		fadeTween.TweenProperty(Vignette!.Material, "shader_parameter/INTENSITY", 5, 5);
		await ToSignal(fadeTween, "finished");

		// Play death animation on player
		Player!.KillPlayer();
	}
}
