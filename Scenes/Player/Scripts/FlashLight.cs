using Godot;
using System;

public partial class FlashLight: SpotLight3D {
	CompressedTexture2D OnTexture = GD.Load<CompressedTexture2D>("res://Assets/Icons/Stats/flashlight_on.svg");
	CompressedTexture2D OffTexture = GD.Load<CompressedTexture2D>("res://Assets/Icons/Stats/flashlight_off.svg");
	
	void TurnOn()
	{
		LightEnergy = 1;
		if (Player!.UI != null)
			Player!.UI.BatteryCharge!.Icon!.Texture = OnTexture;
		DetectionArea!.UpdateVisionCone();
	}

	void TurnOff()
	{
		LightEnergy = 0;
		if (Player!.UI != null)
			Player!.UI.BatteryCharge!.Icon!.Texture = OffTexture;
		DetectionArea!.UpdateVisionCone();
	}
	// Returns if the flashlight is currently active (on), taking into account if the player turned it on and if its charging
	public bool IsActive()
	{
		return LightEnergy != 0;
	}

	[ExportCategory("Configuration")]
	bool on = false;
	[Export] public bool On
	{
		get => on;
		set
		{
			if (!Charging && value)
			{
				TurnOn();
			}
			else if (!Charging && !value)
			{
				TurnOff();
			}
			on = value;
		}
	}
	
	bool charging = false;
	[Export] public bool Charging
	{
		get => charging;
		set
		{
			if (On && !value)
			{
				TurnOn();
			}
			else {
				TurnOff();
			}
			charging = value;
		}
	}

	[Export] public float Charge;
	[Export] public int MaxCharge;
	[Export] public float ChargeDepletionRate = 1;

	[Export] public float MoveDelay;

	bool MoveEffects;

	Node3D? Parent;
	Player? Player;
	Game? Game;

	DetectionArea? DetectionArea;
	
	public override void _Ready()
	{
		TopLevel = true;
		Parent = GetParent<Node3D>();
		DetectionArea = GetNode<DetectionArea>("DetectionArea");
		Charge = MaxCharge;
		Player = GetNode<Player>("../../../");

		Game = GetNode<Game>("/root/Game");
		Game.SettingsUpdated += () => {MoveEffects = Settings.Get<bool>("MoveFX", true);};
	}

	public override void _Process(double delta)
	{
		if (MoveEffects)
		{
			GlobalPosition = GlobalPosition.Lerp(Parent!.GlobalPosition, MoveDelay * (float)delta);
			GlobalRotation = new Vector3(Mathf.LerpAngle(GlobalRotation.X, Parent.GlobalRotation.X, 10 * (float)delta), Mathf.LerpAngle(GlobalRotation.Y, Parent.GlobalRotation.Y, 10 * (float)delta), GlobalRotation.Z);
		} else {
			GlobalPosition = Parent!.GlobalPosition;
			GlobalRotation = Parent.GlobalRotation;
		}
	}

	public override void _PhysicsProcess(double delta) {
		// Deplete charge when on
		if (On && !Charging)
		{
			Charge -= ChargeDepletionRate * (float)delta;
			Charge = Mathf.Clamp(Charge, 0, MaxCharge);
			if (Charge <= 0)
			{
				On = false;
			}
			UpdateUI();
		} else if (Charging) {
			Charge += ChargeDepletionRate * 2 * (float)delta;
			Charge = Mathf.Clamp(Charge, 0, MaxCharge);
			UpdateUI();
		}
	}

	public void UpdateUI()
	{
		Player!.UI?.BatteryCharge!.SetValue((Charge / MaxCharge) * 100, Charging);
	}
	
	void StartChargingFlashLight()
	{
		// Charging = true;
		// Player!.UI?.BatteryCharge!.ProgressBar!.Value = 0;
		// Player!.UI?.BatteryCharge!.Label!.Text = "CHARGING";
	}

	void StopChargingFlashLight()
	{
		Player!.UI?.BatteryCharge!.SetValue((Charge / MaxCharge) * 100, false);
		// Charging = false;
		// float chargePercentage = (Charge / MaxCharge) * 100;
		// Player!.UI?.BatteryCharge!.ProgressBar!.Value = chargePercentage;
		// Player!.UI?.BatteryCharge!.Label!.Text = (int)chargePercentage + "%";
	}

	public override void _Input(InputEvent @event)
	{
		if (@event.IsActionPressed("FlashLightToggle"))
		{
			if (Charge > 0)
			{
				On = !On;
			}
		}
	}
}
