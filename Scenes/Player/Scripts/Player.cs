using Godot;
using System;

using static Godot.GD;

public partial class Player: RigidBody3D {

	#region VariableDeclerations
	[ExportCategory("Movement")]
	[Export] public float MoveSpeed;
	[Export] public float LookSpeed;
	[Export] public float JumpStrength;
	[Export] public float GroundDrag;
	[Export] public float SprintMultiplier;
	[Export] public float AirSpeedMultiplier;
	[Export] public float MaxAirSpeed;
	[Export] public float CrouchStrength;

	Node3D? LookPivot;
	RayCast3D? FloorDetector;
	CollisionShape3D? Collider;

	float ColliderTargetHeight;
	float LookPivotTargetHeight;

	public UI? UI;
	PlayerSanity? Sanity;
	PlayerStamina? Stamina;
	public FlashLight? FlashLight;
	Camera3D? Camera;
	GameProgression? GameProgression;
	Vignette? Vignette;

	#endregion

	#region Signals

	[Signal] public delegate void DiedEventHandler();
	[Signal] public delegate void FlashLightChargeLevelChangedEventHandler();

	#endregion

	#region Init

	public override void _Ready()
	{
		LookPivot = GetNode<Node3D>("LookPivot");
		FloorDetector = GetNode<RayCast3D>("FloorDetector");
		Collider = GetNode<CollisionShape3D>("Collider");
		Sanity = GetNode<PlayerSanity>("PlayerSanity");
		Stamina = GetNode<PlayerStamina>("PlayerStamina");
		FlashLight = GetNode<FlashLight>("LookPivot/Hand/FlashLight");
		Camera = GetNode<Camera3D>("LookPivot/Camera");
		UI = GetNodeOrNull<UI>("/root/Game/UILayer/UIOffset/UI");
		GameProgression = GetNodeOrNull<GameProgression>("/root/Game/GameProgression");
		Vignette = GetNode<Vignette>("ViewLayer/Vignette");

		ColliderTargetHeight = (float)Collider.Shape.Get("height");
		LookPivotTargetHeight = LookPivot.Position.Y;

		CameraShake.Intensity = 0;
	}

	#endregion

	#region Movement

	public override void _Process(double delta)
	{
		// Jump
		if (Input.IsActionJustPressed("MoveJump") && FloorDetector!.IsColliding())
		{
			ApplyCentralImpulse(Vector3.Up * JumpStrength);
		}

		if (Settings.Get<bool>("MoveFX", true))
		{
			// Camera effects
			Vector3 RotatedVelocity = LinearVelocity.Rotated(Vector3.Up, LookPivot!.Rotation.Y);
			Camera!.Fov = Mathf.Lerp(Camera.Fov, 75 + RotatedVelocity.Z, 0.5f);
			Camera.Rotation = new Vector3(0, 0, Mathf.Lerp(Camera.Rotation.Z, -RotatedVelocity.X/150, 0.5f));
		}		
	}

	public override void _PhysicsProcess(double delta)
	{
		Vector2 InputDir;
		Vector3 MoveDir;
		float sprintAdjustment = 1;

		InputDir = Input.GetVector("MoveLeft", "MoveRight", "MoveForward", "MoveBackward").Rotated(-LookPivot!.Rotation.Y);
		
		// Crouching
		if (Input.IsActionPressed("MoveCrouch")) {
			sprintAdjustment = 0.2f;
			Collider!.Shape.Set("height", ColliderTargetHeight - CrouchStrength);
			Collider.Position = new Vector3(Collider.Position.X, (ColliderTargetHeight / 2) - (CrouchStrength / 2), Collider.Position.X);
			LookPivot.Position = LookPivot.Position.Lerp(new Vector3(LookPivot.Position.X, LookPivotTargetHeight - CrouchStrength, LookPivot.Position.Z), 0.1f);
		} else {
			Collider!.Shape.Set("height", ColliderTargetHeight);
			Collider.Position = new Vector3(Collider.Position.Z, ColliderTargetHeight / 2, Collider.Position.Z);
			LookPivot.Position = LookPivot.Position.Lerp(new Vector3(LookPivot.Position.X, LookPivotTargetHeight, LookPivot.Position.Z), 0.1f);
		}

		// Walking
		if (Input.IsActionPressed("MoveSprint") && Stamina!.State != PlayerStamina.StaminaState.FULLRECOVERY)
			sprintAdjustment = SprintMultiplier;
		
		MoveDir = new Vector3(InputDir.X, 0, InputDir.Y) * sprintAdjustment * (float)delta;
		if (!FloorDetector!.IsColliding())
			MoveDir *= AirSpeedMultiplier;
		
		ApplyCentralForce(MoveDir * MoveSpeed);

		if (FloorDetector.IsColliding())
		{
			LinearDamp = GroundDrag;
		}
		else
		{
			LinearDamp = 0;
			LimitSpeed();
		}

		// Stamina drain
		if (Stamina!.State != PlayerStamina.StaminaState.FULLRECOVERY)
		{
			if (Input.IsActionPressed("MoveSprint") && MoveDir != Vector3.Zero)
				Stamina!.State = PlayerStamina.StaminaState.DRAINING;
			else if (Stamina!.Stamina < 100)
				Stamina!.State = PlayerStamina.StaminaState.RECOVERING;
			else
				Stamina!.State = PlayerStamina.StaminaState.DEFAULT;
		}

		// Sanity recovery
		if (MoveDir == Vector3.Zero && Sanity!.State == PlayerSanity.SanityState.NORMAL && FlashLight!.IsActive()) {
			Sanity.State = PlayerSanity.SanityState.RECOVERING;
		} else if ((MoveDir != Vector3.Zero || !FlashLight!.IsActive()) && Sanity!.State == PlayerSanity.SanityState.RECOVERING) {
			Sanity.State = PlayerSanity.SanityState.NORMAL;
		}
	}

	// Clamp the player's horizontal velocity to an acceptable value
	public void LimitSpeed()
	{
		Vector2 flatVelocity = new Vector2(LinearVelocity.X, LinearVelocity.Z);

		// LengthSquared avoids square-root calculation and runs faster.
		if (flatVelocity.LengthSquared() > MaxAirSpeed)
		{
			Vector2 limitedVelocity = flatVelocity.Normalized() * MaxAirSpeed;
			LinearVelocity = new Vector3(limitedVelocity.X, LinearVelocity.Y, limitedVelocity.Y);
		}
	}

	#endregion

	#region MouseLook
	
	public override void _Input(InputEvent inputEvent)
	{
		// Mouse Look
		if (inputEvent is InputEventMouseMotion mouseEvent)
		{
			Vector3 newRotation = GetNode<Node3D>("LookPivot").Rotation;
			newRotation.X -= Mathf.DegToRad(mouseEvent.Relative.Y * LookSpeed);
			newRotation.X = Mathf.Clamp(newRotation.X, -Mathf.Pi/2, Mathf.Pi/2);
			newRotation.Y -= Mathf.DegToRad(mouseEvent.Relative.X * LookSpeed);
			LookPivot!.Rotation = newRotation;

			if (Settings.Get<bool>("MoveFX", true))
			{
				Camera!.Rotation += new Vector3(0, 0, -(Mathf.DegToRad(mouseEvent.Relative.X) / 8));
			}
		}

		// Debug code, should not appear in final game!!
		// If you can read this, Raf is an idiot
		if (inputEvent is InputEventKey keyEnv)
		{
			if (keyEnv.Keycode == Key.K)
			{
				KillPlayer();
			}
		}
	}

	#endregion

	#region Death

	public async void KillPlayer()
	{
		Game.GameState = Game.GameStates.PREVENTPAUSE;

		// Show monster
		GetNode<CanvasItem>("ViewLayer/NecromancerView").Visible = true;

		AnimationPlayer DeathAnim = GetNode<AnimationPlayer>("DeathAnimation");
		DeathAnim.Play("Died");
		await ToSignal(DeathAnim, "animation_finished");

		// End game
		EmitSignal("Died");
	}

	#endregion
}
