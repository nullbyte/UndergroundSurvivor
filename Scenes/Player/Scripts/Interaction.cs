using Godot;
using System;

public partial class Interaction: RayCast3D {
	Game? Game;
	Player? Player;
	FlashLight? FlashLight;

	public override void _Ready()
	{
		Game = GetNode<Game>("/root/Game");
		Player = GetNode<Player>("../../../");
		FlashLight = GetNode<FlashLight>("../../Hand/FlashLight");
	}
	
	public override void _Input(InputEvent @event)
	{
		if (@event.IsActionPressed("Use"))
		{
			if (IsColliding())
			{
				if (GetCollider() is IInteractable item)
				{
					item.Activated(Player!);
				}
			}
		}
	}
}
