using Godot;
using System;

public partial class CameraShake : Camera3D
{
	// Shake positions per second
	[Export] int ShakeSpeed = 10;
	public static float Intensity = 0f;
	Random rng = new();

	float CurrentShakeDuration;
	Vector3 newRotation = Vector3.Zero;

	// Shake camera
	public override void _Process(double delta)
	{
		if (CurrentShakeDuration > ShakeSpeed/60)
		{
			newRotation.X = ((float)(rng.Next() % 10) / 1000) * Intensity;
			newRotation.Y = ((float)(rng.Next() % 10) / 1000) * Intensity;
		}
		
		Rotation = newRotation;

		CurrentShakeDuration += (float)delta;
	}
}
