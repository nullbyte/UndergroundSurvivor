using Godot;
using System;

public partial class OrbLabel : Label
{
	Random rng = new();

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		var newPosition = new Vector2();
		newPosition.X = (float)(rng.Next() % 10) / 10;
		newPosition.Y = (float)(rng.Next() % 10) / 10;
		
		Position = newPosition;
	}
}
