using Godot;
using System;

public partial class TrainModel : StaticBody3D
{
	void OpenDoors()
	{
		GetNode<AnimationPlayer>("Animation").Play("OpenDoors");
	}
}
