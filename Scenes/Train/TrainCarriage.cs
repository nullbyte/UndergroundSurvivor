using Godot;
using System;

public partial class TrainCarriage : Area3D
{
	[Export] int Number;
	
	// Set active carriage to the current players position, used to determine enemy spawn frequency
	void OnEnter(Node3D body)
	{
		if (body is Player)
		{
			Train.ActiveCarriage = Number;
		}
	}
}
