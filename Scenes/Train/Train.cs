using Godot;
using System;

public partial class Train : StaticBody3D
{
	[Signal] public delegate void DoorsOpenEventHandler();

	public static int ActiveCarriage;

	public override void _Ready()
	{
		GameProgression? GameProgression = GetNodeOrNull<GameProgression>("/root/Game/GameProgression");
		// If train is in the title scene, don't connect signal
		if (GameProgression != null)
		{
			GameProgression.GameEvent += OnGameEvent;
		}
	}
	
	public void UpdateReflections()
	{
		GetNode<ReflectionProbe>("ReflectionProbe").UpdateMode = ReflectionProbe.UpdateModeEnum.Once;
		GetNode<ReflectionProbe>("ReflectionProbe2").UpdateMode = ReflectionProbe.UpdateModeEnum.Once;
	}

	void OnGameEvent(int id)
	{
		UpdateReflections();
		
		if ((GameProgression.Events)id == GameProgression.Events.STATION)
		{
			EmitSignal("DoorsOpen");
		}
		else if ((GameProgression.Events)id == GameProgression.Events.TRAIN)
		{
			GetNodeOrNull<FogVolume>("FogVolume")?.QueueFree();
		}
	}
}
