using Godot;
using System;

public partial class GameOver : Control
{
	[Signal] public delegate void RespawnEventHandler();
	
	public void Activate()
	{
		GetTree().Paused = true;
		Game.GameState = Game.GameStates.OVER;
		Game.SkipIntro = true;
		Input.MouseMode = Input.MouseModeEnum.Visible;

		Visible = true;
		GetNode<Label>("Label").Text = GetUserName() + " Passed Away";
		GetNode<AnimationPlayer>("Animation").Play("Show");
	}

	void Retry()
	{
		GetTree().Paused = false;
		GetTree().ChangeSceneToFile("res://Scenes/Game/Game.tscn");
	}

	void Quit()
	{
		GetTree().Paused = false;
		GetTree().ChangeSceneToFile("res://Scenes/Title/Title.tscn");
	}

	string GetUserName()
	{
		string username = System.Environment.UserName;

		if (username.Contains('.'))
		{
			return username.Split('.')[0];
		}
		else if (String.IsNullOrWhiteSpace(username))
		{
			return "You";
		}
		return username;
	}
}
