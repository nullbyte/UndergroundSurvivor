using Godot;
using System;

public partial class MainMenu : Control
{
	[Signal] public delegate void StartGameEventHandler();
	[Signal] public delegate void SettingsPressedEventHandler();
	[Signal] public delegate void CreditsPressedEventHandler();

	public void ShowAnim()
	{
		GetNode<AnimationPlayer>("Animation").Play("Show");
	}
	
	void StartButtonPressed()
	{
		GetNode<AnimationPlayer>("Animation").Play("Hide");
		EmitSignal("StartGame");
	}

	void SettingsButtonPressed() => EmitSignal("SettingsPressed");
		// GetNode<SettingsMenu>("Settings").LoadSettings();
		// GetNode<AnimationPlayer>("Settings/Animation").Play("Show");

	void CreditsButtonPressed() => EmitSignal("CreditsPressed");
		// GetNode<AnimationPlayer>("Credits/Animation").Play("Show");

	void QuitButtonPressed()
	{
		GetNode<Window>("QuitDialog").Show();
	}

	void QuitConfirmed()
	{
		GetTree().Quit();
	}

	void StatsHover()
	{
		GetTree().CreateTween().TweenProperty(GetNode("StatsButton"), "position", new Vector2(1040, 552), 0.2);
	}

	void StatsUnhover()
	{
		GetTree().CreateTween().TweenProperty(GetNode("StatsButton"), "position", new Vector2(1104, 552), 0.2);
	}

	void StatsInput(InputEvent e)
	{
		if (e is InputEventMouseButton b && !b.Pressed)
		{
			GetTree().ChangeSceneToFile("res://Scenes/UI/StatsPage/StatsPage.tscn");
		}
	}
}
