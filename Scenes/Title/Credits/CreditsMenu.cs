using Godot;
using System;

public partial class CreditsMenu : Control
{
	void Close()
	{
		GetNode<AnimationPlayer>("Animation").Play("Hide");
	}

	void Open()
	{
		GetNode<AnimationPlayer>("Animation").Play("Show");
	}
}