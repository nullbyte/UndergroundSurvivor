using Godot;
using System;

public partial class Title: Node {
	[Export] Camera3D? Camera;
	[Export] float MoveSensitivity;

	AnimationPlayer? Animation;
	MainMenu? MainMenu;
	
	// Show title
	public override async void _Ready()
	{
		MainMenu = GetNode<MainMenu>("UILayer/MainMenu");
		Animation = GetNode<AnimationPlayer>("StartAnimation");
		Animation!.Play("Show");
		GetNode<MainMenu>("UILayer/MainMenu").ShowAnim();

		await ToSignal(Animation, "animation_finished");
		
		GetNode<ColorRect>("UILayer/FadeBlack").Visible = false;
		
		Input.MouseMode = Input.MouseModeEnum.Visible;
	}

	// Start move-back animation
	void StartGame()
	{
		StatsManager.Stats["NewGames"]++;

		Animation!.Play("StartGame");
		var cameraTween = GetTree().CreateTween();
		cameraTween.TweenProperty(Camera!, "rotation", Vector3.Zero, 3f);
	}

	// Actually load the game scene
	void LoadGame()
	{
		GetTree().ChangeSceneToFile("res://Scenes/Game/Game.tscn");
	}

    public override void _Process(double delta)
    {
		var windowSize = GetViewport().GetVisibleRect().Size;
		var mousePos = GetViewport().GetMousePosition();
		var windowCenter = windowSize / 2;

		var distFromCenter = (windowCenter - mousePos) * (MoveSensitivity / 10000);
		var targetRotation = new Vector3(distFromCenter.Y, distFromCenter.X, 0);

		Camera!.Rotation = Camera.Rotation.Lerp(targetRotation, 0.01f);
		MainMenu!.Position = MainMenu.Position.Lerp(distFromCenter * 100, 0.1f);
    }
}
