using Godot;
using System;

public partial class SettingsMenu: Control
{
	public void LoadSettings()
	{
		Settings.Load();
		foreach (var node in GetNode("SettingsPanel/SettingsItems").GetChildren())
		{
			var Setting = (SettingsItem)node;
			bool value = Setting.Value = Settings.Get<bool>(Setting.Name, Setting.Default);
		}
	}
	
	public void SaveSettings()
	{
		foreach (var node in GetNode("SettingsPanel/SettingsItems").GetChildren())
		{
			var Setting = (SettingsItem)node;
			string key = Setting.Name;
			bool value = Setting.Value;
			Settings.Set(key, value);
		}
		Settings.Save();
	}

	void ApplySettings()
	{
		SaveSettings();
		SettingsHelper.SettingsUpdated();
		Close();
	}

	void Close()
	{
		GetNode<AnimationPlayer>("Animation").Play("Hide");
	}

	void Open()
	{
		LoadSettings();
		GetNode<AnimationPlayer>("Animation").Play("Show");
	}
}
