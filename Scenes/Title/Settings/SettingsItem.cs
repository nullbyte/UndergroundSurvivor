using Godot;
using System;

public partial class SettingsItem: HBoxContainer {
	public bool Value {
		get {
			return GetNode<CheckButton>("Toggle").ButtonPressed;
		}
		set {
			GetNode<CheckButton>("Toggle").ButtonPressed = value;
		}
	}

	bool defaultval;
	[Export] public bool Default {
		get {
			return defaultval;
		}
		set {
			defaultval = value;
			Value = value;
		}
	}
}
