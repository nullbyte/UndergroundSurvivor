using Godot;
using System;
using System.Threading.Tasks;

public partial class BootSplash : Control
{
	// List of images to show on game start
	[Export] Texture2D[]? Textures;

	public override async void _Ready() {
		// Load settings and clamp mouse
		SettingsHelper.SettingsUpdated();
		Input.MouseMode = Input.MouseModeEnum.Captured;

		// Wait for dramatic effect
		await ToSignal(GetTree().CreateTimer(0.5), "timeout");

		TextureRect TextureDisplay = GetNode<TextureRect>("TextureDisplay");
		AnimationPlayer Animation = GetNode<AnimationPlayer>("Animation");

		// Loop through textures and animate them
		foreach (var tex in Textures!) {
			TextureDisplay.Texture = tex;

			Animation.Play("FadeIn");

			await ToSignal(Animation, "animation_finished");
		}

		// Show intro message
		var messagesdata = FileAccess.Open("res://Scenes/BootSplash/messages.txt", FileAccess.ModeFlags.Read).GetAsText();
		var messages = messagesdata.Split('\n');
		var messagenumber = Random.Shared.Next() % (messages.Length - 1);
		GetNode<Label>("IntroMessage").Text = messages[messagenumber];
		Animation.Play("ShowMessage");
		await ToSignal(Animation, "animation_finished");

		// Load title scene
		GetTree().ChangeSceneToFile("res://Scenes/Title/Title.tscn");
	}
}
