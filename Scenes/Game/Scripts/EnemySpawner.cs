using System;
using System.Linq;
using Godot;
using System.Collections.Generic;

public partial class EnemySpawner: Node3D {
	
	// Monster spawn control
	public bool Spawning
	{
		get
		{
			return SpawnTimer!.IsStopped();
		}
		set
		{
			if (value) SpawnTimer!.Start();
			else SpawnTimer!.Stop();
		}
	}

	// Difficulty counter
	float MinWaitTime = 1f;
	float BaseWaitTime = 8;
	
	PackedScene EnemyScene = GD.Load<PackedScene>("res://Scenes/Monster/MonsterWindow/MonsterWindow.tscn");
	List<Node3D> Carriages = new();
	Random Rng = new();
	Timer? SpawnTimer;
	[Export] Train? Train;

	
	public override void _Ready()
	{
		foreach (var item in GetChildren())
		{
			if (item is Node3D item3D)
			{
				Carriages.Add(item3D);
			}
		}

		SpawnTimer = GetNode<Timer>("SpawnTimer");
		UpdateDifficulty();

		// Kill all enemies when the game event changes
		GetNode<GameProgression>("/root/Game/GameProgression").GameEvent += (id) => {
			KillAllEnemies();
			
			if ((GameProgression.Events)id == GameProgression.Events.TRAIN)
				Spawning = true;
			else
				Spawning = false;
		};
	}

	// Create new enemy at any available window
	void SpawnEnemy()
	{
		// Increase likelihood of spawning in the players current carriage;
		int spawningCarriage = 0;
		int rngSeed = Rng.Next();

		if (rngSeed % 3 != 0)
			spawningCarriage = Train.ActiveCarriage;
		else
			spawningCarriage = rngSeed % 2;
		
		foreach (var point in Carriages[spawningCarriage].GetChildren().OrderBy(_ => Rng.Next()))
		{
			if (point is Node3D spawner && point.GetChildCount() == 0)
			{
				Node3D newEnemy = EnemyScene.Instantiate<Node3D>();
				spawner.AddChild(newEnemy);
				newEnemy.GlobalPosition = spawner.GlobalPosition;
				break;
			}
		}

		CheckForAmbush();
		UpdateDifficulty();
	}

	// If monsters are present on all four windows of a carriage, break in and kill player.
	void CheckForAmbush()
	{
		foreach (var carriage in Carriages)
		{
			int numberOfEnemies = 0;
			foreach (var point in carriage.GetChildren())
			{
				if (point.GetChildCount() >= 1)
				{
					numberOfEnemies++;
				}
			}
			if (numberOfEnemies >= 4)
			{
				Ambush();
			}
		}
	}

	// Increase spawn frequency over time
	void UpdateDifficulty()
	{
		float time = GetNode<Clock>("/root/Game/Clock").Time;
		float waitTime = BaseWaitTime - ((time - 60) / 40);
		if (waitTime < MinWaitTime) BaseWaitTime = MinWaitTime;
		SpawnTimer!.WaitTime = waitTime;
	}

	// Kill player on ambush
	async void Ambush()
	{
		if (Game.GameState != Game.GameStates.PREVENTPAUSE)
		{
			GetNode<Game>("/root/Game").ShowMessage("They got in");
			Spawning = false;
			KillAllEnemies();

			await ToSignal(GetTree().CreateTimer(1), "timeout");
			GetNode<Player>("/root/Game/Player").KillPlayer();
		}
	}

	// Remove all enemies from the train
	void KillAllEnemies()
	{
		foreach (var carriage in Carriages)
		{
			foreach (var spawnPoint in carriage.GetChildren())
			{
				if (spawnPoint.GetChildren().Count > 0)
				{
					if (spawnPoint.GetChild(0) is MonsterWindow monster)
					{
						monster.Seen();
					}
				}
			}
		}
	}
}
