using Godot;
using System;

public partial class GroundAnimation : AnimationPlayer
{
	public override void _Ready()
	{
		// When settings update, toggle animation
		GetNode<Game>("/root/Game").SettingsUpdated += () =>
		{
			if (Settings.Get<bool>("MoveFX", true))
			{
				Play("Shake");
			}
			else
			{
				Stop();
			}
		};

		GetNode<GameProgression>("/root/Game/GameProgression").GameEvent += async (id) =>
		{
			if ((GameProgression.Events)id == GameProgression.Events.STATION)
			{
				await ToSignal(this, "animation_finished");
				Stop();
			}
		};
	}
}
