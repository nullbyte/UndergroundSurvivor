using System;
using Godot;

public partial class NightProgression: AnimationPlayer
{
	// Reference to game node
	Game? Game;

	public override void _Ready()
	{
		Game = GetNode<Game>("/root/Game");
		
		CurrentAnimation = "ToNight";

		// Activate monsters when the tutorial ends
		GetNode<GameProgression>("/root/Game/GameProgression").GameEvent += async (id) =>
		{
			if ((GameProgression.Events)id == GameProgression.Events.TRAIN)
			{
				Game.ShowMessage("Something is outside the train");
			}
			else if ((GameProgression.Events)id == GameProgression.Events.ENDTRAIN)
			{
				Game.ShowMessage("The darkness subsides");
			}

			if ((GameProgression.Events)id == GameProgression.Events.TUTORIAL)
			{
				Play("ToNight");
				await ToSignal(this, "animation_finished");
				GetNode<GameProgression>("/root/Game/GameProgression").EmitSignal("GameEvent", (int)GameProgression.Events.TRAIN);
			}
			else
			{
				if (CurrentAnimationPosition != CurrentAnimationLength)
					Seek(CurrentAnimationLength, true);
			}
		};
	}
}
