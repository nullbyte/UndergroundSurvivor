using Godot;
using System;

public partial class ObjectSpawner: Node3D
{
	[Export] PackedScene? SceneData;
	Timer? Timer;
	
	bool Active
	{
		get
		{
			return Timer!.Paused;
		}
		set
		{
			Timer!.Paused = !value;
		}
	}
	
	Random rand = new();

	public override void _Ready()
	{
		// // Delay spawning of right side to create alternation
		// if (((String)Name).EndsWith("Right")) {
		// 	await ToSignal(GetTree().CreateTimer(1), "timeout");
		// }
		Timer = GetNode<Timer>("Timer");
		Timer.Start();

		GetNode<GameProgression>("/root/Game/GameProgression").GameEvent += (id) =>
		{
			if ((GameProgression.Events)id is GameProgression.Events.TUTORIAL or GameProgression.Events.TRAIN)
			{
				Active = true;
			} else
			{
				Active = false;
			}
		};
	}

	void SpawnObject()
	{
		var Object = SceneData!.Instantiate<Node3D>();
		AddChild(Object);
		Object.GlobalPosition = GlobalPosition;
		UpdateSpawnDelay();
	}

	void UpdateSpawnDelay()
	{
		// Change spawn time based on train speed, making sure not to div by zero to please cookie monster.
		float adjustedTime;
		adjustedTime = Environment.TrainSpeed != 0 ? Environment.DefaultTrainSpeed / Environment.TrainSpeed : 0;
		float randomTime = 1;

		// If this is a tree spawner, randomize spawn delay
		if (((String)Name).StartsWith("Tree"))
		{
			// Set delay to a float from 0.1 to 1.0
			randomTime = (((rand.Next() % 9) + 1) / 10f);
		}
		
		float newTime = randomTime * adjustedTime;
		
		if (newTime > 0)
			GetNode<Timer>("Timer").WaitTime = randomTime * adjustedTime;
		else
			GetNode<Timer>("Timer").Paused = true;
	}
}
