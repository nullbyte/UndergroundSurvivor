using Godot;
using System;
using System.Threading.Tasks;

public partial class Game: Node
{
	// Signals
	[Signal] public delegate void SettingsUpdatedEventHandler();
	[Signal] public delegate void GameStartedEventHandler();
	[Signal] public delegate void ChargerActivatedEventHandler();
	[Signal] public delegate void DoorActivatedEventHandler();

	// Global variables
	public static GameStates GameState = GameStates.PLAYING;
	public static bool SkipIntro = false;

	public enum GameStates
	{
		PLAYING,
		PREVENTPAUSE,
		PAUSED,
		OVER
	}

	// Game events
	public override void _Ready()
	{
		if (OS.HasFeature("editor"))
		{
			GD.Print("Running in editor");
			
			Settings.Load();
		}

		SettingsUpdated += SettingsHelper.SettingsUpdated;
		EmitSignal("SettingsUpdated");

		GameState = GameStates.PLAYING;
		Input.MouseMode = Input.MouseModeEnum.Captured;

		// Skip intro if user selected settings option
		if (SkipIntro || Settings.Get<bool>("SkipIntro", false))
			GetNodeOrNull<GameProgression>("GameProgression")?.EmitSignal("GameEvent", (int)GameProgression.Events.TRAIN);
		else
			GetNodeOrNull<GameProgression>("GameProgression")?.EmitSignal("GameEvent", (int)GameProgression.Events.TUTORIAL);
	}

	// Save on quit
	public override void _ExitTree()
	{
		StatsManager.UpdateStats();
	}

	// Show a hint dialog in the top right
	public async Task ShowHint(string message)
	{
		if (Settings.Get<bool>("Hints", true) && GetNodeOrNull<HintDisplay>("UILayer/UIOffset/HintDisplay") != null)
			await GetNodeOrNull<HintDisplay>("UILayer/UIOffset/HintDisplay").ShowHint(message);
	}

	// Cancel an existing hint dialog
	public void CompleteHint(string? message = null)
	{
		if (Settings.Get<bool>("Hints", true))
			GetNodeOrNull<HintDisplay>("UILayer/UIOffset/HintDisplay")?.CloseHint(message);
	}

	// Show a temporary message which fades across the screen
	public void ShowMessage(string message)
	{
		GetNodeOrNull<GameMessage>("UILayer/UIOffset/GameMessage")?.ShowMessage(message);
	}
}
