using Godot;
using System.Collections.Generic;

public partial class GameProgression : Node
{
	[Signal] public delegate void GameEventEventHandler(int EventId);

	// All key moments in the game
	public enum Events
	{
		TUTORIAL,
		TRAIN,
		ENDTRAIN,
		STATION,
	}

	// The times at which these events take place
	public static Dictionary<int, Events> EventTimes = new()
	{
		{1, Events.TUTORIAL},
		{60, Events.TRAIN},
		{300, Events.ENDTRAIN},
	};

	void TimeUpdated(int time)
	{
		// Thank goodness ContainsKey runs in O(1) time, performance shall survive
		if (EventTimes.ContainsKey(time))
		{
			StartEvent(EventTimes[time]);
		}
	}

	public void StartEvent(Events eventId)
	{
		EmitSignal("GameEvent", (int)eventId);
	}

	public override void _Input(InputEvent e)
	{
		if (e is InputEventKey k)
		{
			if (k.Keycode == Key.L && OS.HasFeature("editor"))
			{
				GetNode<Clock>("../Clock").Time = 299;
			}
		}
	}
}
