using Godot;
using System;

public partial class Ground : MeshInstance3D
{
	double GroundOffset = 0;

	public override void _Process(double delta)
	{
		GroundOffset += delta * -Environment.TrainSpeed;

		MaterialOverride.Set("shader_parameter/Offset", GroundOffset);
	}
}
