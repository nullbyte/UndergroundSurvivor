using Godot;
using System;

public partial class StationSpawner : Node3D
{
	[Export] PackedScene? StationScene;

	GameProgression? GameProgression;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GameProgression = GetNode<GameProgression>("/root/Game/GameProgression");
		GameProgression.GameEvent += (id) =>
		{
			// On station event, create new station
			if ((GameProgression.Events)id == GameProgression.Events.ENDTRAIN)
			{
				GetNode<AnimationPlayer>("StationAnimation").Play("Start");
			}
		};
	}

	void SpawnStation()
	{
		Node3D station = StationScene!.Instantiate<Node3D>();
		AddChild(station);
		station.GlobalPosition = GlobalPosition;
	}

	void AnimationFinish() => GameProgression!.EmitSignal("GameEvent", (int)GameProgression.Events.STATION);
}
