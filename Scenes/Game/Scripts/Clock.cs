using Godot;
using System;

public partial class Clock : Timer
{
	[Signal] public delegate void TimeUpdatedEventHandler(int time);

	public int Time;
	
	public override void _Ready()
	{
		GetNode<GameProgression>("/root/Game/GameProgression").GameEvent += (id) =>
		{
			// Do an inverse lookup in the game events list and set time
			foreach (var gameevent in GameProgression.EventTimes)
			{
				if ((GameProgression.Events)id == gameevent.Value)
				{
					Time = gameevent.Key;
					break;
				}
			}
		};
	}
	
	void TimeProgress()
	{
		Time++;
		EmitSignal("TimeUpdated", Time);
	}
}
