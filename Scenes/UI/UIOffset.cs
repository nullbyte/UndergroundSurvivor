using Godot;
using System;

public partial class UIOffset : Control
{
	bool MoveEffects = true;

	public override void _Ready() {
		GetNode<Game>("/root/Game").SettingsUpdated += () => {
			MoveEffects = Settings.Get<bool>("MoveFX", true);
		};
	}

	public override void _Process(double delta) {
		if (MoveEffects) {
			Vector2 MouseVelocity = Input.GetLastMouseVelocity();
			Position = Position.Lerp(-MouseVelocity/200, 0.1f);
		}
	}
}
