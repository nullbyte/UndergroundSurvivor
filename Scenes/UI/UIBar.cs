using Godot;
using System;

public partial class UIBar: Control {
	public ProgressBar? ProgressBar;
	public Label? Label;
	public Timer? VisibilityTimer;

	[Export] Color DefaultColor;
	[Export] Color WarningColor;

	public override void _Ready() {
		ProgressBar = GetNode<ProgressBar>("ProgressBar");
		Label = GetNodeOrNull<Label>("ProgressBar/Label");
		VisibilityTimer = GetNode<Timer>("VisibilityTimer");
	}

	Color Opaque = new Color(1, 1, 1, 1);
	Color Translucent = new Color(1, 1, 1, 0.25f);
	
	public void SetValue(float value) {
		ProgressBar!.Value = value;
		Modulate = Opaque;

		if (Label != null)
			Label.Text = $"{(int)value}%";
		VisibilityTimer!.Start();
	}

	void FadeOut() {
		Tween fadeAnimation = CreateTween();
		fadeAnimation.TweenProperty(this, "modulate", Translucent, 0.5f);
	}
}
