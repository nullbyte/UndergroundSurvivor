using Godot;
using System;

public partial class Pause: Control {
	AnimationPlayer? Animation;
	
	public override void _Ready()
	{
		Input.MouseMode = Input.MouseModeEnum.Captured;
		Visible = false;

		Animation = GetNode<AnimationPlayer>("Animation");
	}
	
	public override void _Input(InputEvent @event)
	{
		if (@event.IsActionPressed("WindowEscape")) {
			if (Game.GameState == Game.GameStates.PAUSED)
				UnPause();
			else if (Game.GameState == Game.GameStates.PLAYING)
				StartPause();
		}
	}
	
	void StartPause()
	{
		GetTree().Paused = !GetTree().Paused;
		Game.GameState = Game.GameStates.PAUSED;
		Input.MouseMode = Input.MouseModeEnum.Visible;
		Animation!.Play("Pause");
	}

	void UnPause()
	{
		GetTree().Paused = !GetTree().Paused;
		Game.GameState = Game.GameStates.PLAYING;
		Input.MouseMode = Input.MouseModeEnum.Captured;
		Animation!.Play("Unpause");
	}

	async void QuitToTitle()
	{
		StatsManager.UpdateStats();
		Animation!.Play("Quit");
		await ToSignal(Animation, "animation_finished");
		GetTree().Paused = false;
		GetTree().ChangeSceneToFile("res://Scenes/Title/Title.tscn");
	}
}
