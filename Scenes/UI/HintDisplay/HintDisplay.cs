using Godot;
using System;
using System.Threading.Tasks;

public partial class HintDisplay: Control {
	Game? Game;
	AnimationPlayer? Animation;
	Label? MessageLabel;
	bool Active = false;

	// Allow awaiting the dialog
	TaskCompletionSource HintTask = new();

	public override void _Ready() {
		Game = GetNode<Game>("/root/Game");
		Animation = GetNode<AnimationPlayer>("Animation");
		MessageLabel = GetNode<Label>("Origin/Panel/Message");

		Visible = false;
	}

	public async Task ShowHint(string message) {
		if (Active) {
			Animation!.Play("Hide");
			await ToSignal(Animation, "animation_finished");
		}
		MessageLabel!.Text = message;
		Animation!.Play("Show");
		Active = true;

		// Wait until the hint is closed or completed
		HintTask = new();
		await HintTask.Task;
	}

	public void CloseHint(string? message) {
		if (Active && (String.IsNullOrEmpty(message) || MessageLabel!.Text == message)) {
			Active = false;
			Animation!.Play("Hide");
			HintTask.SetResult();
		}
	}

	public override void _Input(InputEvent ev) {
		if (ev.IsActionPressed("CloseHint")) {
			if (Active) {
				Animation!.Play("Hide");
				Active = false;
				HintTask.SetResult();
			}
		}
	}
}
