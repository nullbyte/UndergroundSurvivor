using Godot;
using System;

public partial class FPSCounter: Label {
	Timer? UpdateTimer;

	public override void _Ready() {
		UpdateTimer = GetNode<Timer>("UpdateTimer");
		GetNode<Game>("/root/Game").SettingsUpdated += OnSettingsUpdated;
		OnSettingsUpdated();
	}

	void OnSettingsUpdated() {
		if (Settings.Get<bool>("FPS", false))
			Enable();
		else
			Disable();
	}
	
	void Enable() {
		Visible = true;
		UpdateTimer!.Start();
	}

	void Disable() {
		Visible = false;
		UpdateTimer!.Stop();
	}

	void Update() {
		Text = Engine.GetFramesPerSecond() + " FPS";
	}
}
