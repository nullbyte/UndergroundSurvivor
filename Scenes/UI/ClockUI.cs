using Godot;
using System;

public partial class ClockUI : Label
{
	[Export] int InitialTime;

	public void UpdateTime(int time)
	{
		Text = IntToTime(time);
	}

	string IntToTime(int time)
	{
		string hours = ((((time/60) + InitialTime)%12)+1).ToString();
		string minutes = (time%60).ToString();
		if (minutes.Length == 1) {
			minutes = "0" + minutes;
		}
		return $"{hours}:{minutes}";
	}
}
