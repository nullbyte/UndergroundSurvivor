using Godot;
using System;

public partial class ClockUIController : Control
{
	[Export] float HoldDelay;

	Label? Clock;
	TextureProgressBar? LoadingBar;

	public override void _Ready() {
		Clock = GetNode<Label>("Clock");
		LoadingBar = GetNode<TextureProgressBar>("LoadingBar");
	}

	public override void _Process(double delta) {
		float oldBarOpacity = LoadingBar!.Modulate.A;
		float oldClockOpacity = Clock!.Modulate.A;

		if (Input.IsActionPressed("ShowClock") && LoadingBar!.Value >= LoadingBar!.MaxValue) {
			// If player has held tab for enough time, show clock and hide prog bar
			LoadingBar!.Modulate = new Color(1, 1, 1, Mathf.Lerp(oldBarOpacity, 0, 5 * (float)delta));
			Clock!.Modulate = new Color(1, 1, 1, Mathf.Lerp(oldClockOpacity, 1, 8 * (float)delta));
		} else if (Input.IsActionPressed("ShowClock")) {
			// If player is holding tab, increase prog bar value and opacity
			LoadingBar!.Value += HoldDelay * delta;
			
			LoadingBar!.Modulate = new Color(1, 1, 1, Mathf.Lerp(oldBarOpacity, 1, 5 * (float)delta));
			Clock!.Modulate = new Color(1, 1, 1, Mathf.Lerp(oldClockOpacity, 0, 8 * (float)delta));
		} else {
			// else hide prog bar, and clock, and decrease prog bar value
			LoadingBar!.Value -= HoldDelay * 2 * delta;

			LoadingBar!.Modulate = new Color(1, 1, 1, Mathf.Lerp(oldBarOpacity, 0, 5 * (float)delta));
			Clock!.Modulate = new Color(1, 1, 1, Mathf.Lerp(oldClockOpacity, 0, 8 * (float)delta));
		}
		// Clamp value
		LoadingBar!.Value = Mathf.Clamp((float)LoadingBar!.Value, 0f, 100f);
	}
}
