using Godot;
using System;

public partial class StatsPage : Control
{
	[Export] Control? KeysList;
	[Export] Control? ValuesList;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Environment.TrainSpeed = Environment.DefaultTrainSpeed;
		GetNode<AnimationPlayer>("Info/Background/DefaultProjectIcon/Animation").Play("Sway");
		GetNode<AnimationPlayer>("Info/Background/SkyView/SkyViewport/Camera3D/WorldEnvironment/SkyAnimation").Play("ToNight");

		UpdateStats();
	}

	void UpdateStats()
	{
		foreach (var item in StatsManager.Stats.Keys)
		{
			Label statLabel = new();
			statLabel.Text = item;
			KeysList!.AddChild(statLabel);


			Label resLabel = new();
			resLabel.Text = Settings.Get<int>(item, 0, true).ToString();
			ValuesList!.AddChild(resLabel);
		}
	}

	void BackToMenu()
	{
		GetTree().ChangeSceneToFile("res://Scenes/Title/Title.tscn");
	}
}
