using Godot;
using System;

public partial class BatteryCharge: Control {
	public ProgressBar? ProgressBar;
	public Label? Label;
	public TextureRect? Icon;
	public Timer? VisibilityTimer;


	Color Opaque = new Color(1, 1, 1, 1);
	Color Translucent = new Color(1, 1, 1, 0.25f);
	
	public override void _Ready() {
		ProgressBar = GetNode<ProgressBar>("ProgressBar");
		Label = GetNode<Label>("ProgressBar/Label");
		Icon = GetNode<TextureRect>("FlashLightIcon");
		VisibilityTimer = GetNode<Timer>("VisibilityTimer");
	}

	public void SetValue(float chargePercentage, bool charging) {
		Modulate = Opaque;
		ProgressBar!.Value = chargePercentage;
		Label!.Text = (int)chargePercentage + "%" + (charging? " (Charging)" : "");

		VisibilityTimer!.Start();
	}
	
	void FadeOut() {
		Tween fadeAnimation = CreateTween();
		fadeAnimation.TweenProperty(this, "modulate", Translucent, 0.5f);
	}
}
