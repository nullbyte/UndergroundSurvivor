using Godot;
using System;

public partial class UI: Control
{
	public BatteryCharge? BatteryCharge;
	public UIBar? SanityBar;
	public UIBar? StaminaBar;
	public ClockUI? Clock;

	// Accessor variables for easy referencing
	public override void _Ready()
	{
		BatteryCharge = GetNode<BatteryCharge>("BatteryCharge");
		SanityBar = GetNode<UIBar>("SanityBar");
		StaminaBar = GetNode<UIBar>("StaminaBar");
		Clock = GetNode<ClockUI>("Clock");
		GetNode<AnimationPlayer>("Animation").Play("Show");
	}

	// Capture time update signal and pass to the clock ui
	void TimeUpdated(int time)
	{
		Clock!.UpdateTime(time);
	}
}
