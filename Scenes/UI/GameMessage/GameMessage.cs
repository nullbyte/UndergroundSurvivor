using Godot;
using System;

public partial class GameMessage: Control {
	Label? MessageLabel;
	AnimationPlayer? Animation;

	public override void _Ready() {
		MessageLabel = GetNode<Label>("MessageCanvas/Message");
		Animation = GetNode<AnimationPlayer>("Animation");
		GetNode<CanvasItem>("MessageCanvas").Modulate = new Color(255, 255, 255, 0);
	}

	public async void ShowMessage(string message) {
		MessageLabel!.Text = message;
		Animation!.Play("Show");
		await ToSignal(GetTree().CreateTimer(3), "timeout");
		Animation.Play("Hide");
	}
}
