// 1AM is not a good time to be writing shaders
// Who knew that mix() was a function, I sure didn't until now
// that's 3 hours of my life I'll never get back

shader_type spatial;

uniform sampler2D Texture1 : source_color;
uniform sampler2D Texture2 : source_color;
uniform sampler2D Normal1 : hint_normal;
uniform sampler2D Normal2 : hint_normal;
uniform sampler2D Roughness : hint_roughness_gray;
uniform sampler2D BlendTexture;
uniform sampler2D TerrainTexture;
uniform sampler2D TerrainIntensityTexture;
uniform sampler2D DepthTexture : hint_depth_texture;
uniform float TerrainIntensity : hint_range(0.0, 30.0, 0.1);
uniform float TextureUVScale : hint_range(1.0, 500.0, 1);
uniform float BlendUVScale : hint_range(1.0, 500.0, 1);
uniform float Offset;
uniform float SurfaceSize : hint_range(1.0, 500.0, 1);

void fragment() {
	float adjustmentAmount = Offset / SurfaceSize;
	vec2 adjustedUV = vec2(UV.x + adjustmentAmount, UV.y);

	vec4 mixColor = texture(BlendTexture, adjustedUV * BlendUVScale);
	
	// Albedo
	vec4 tex1Color = texture(Texture1, adjustedUV * TextureUVScale);
	vec4 tex2Color = texture(Texture2, adjustedUV * TextureUVScale);
	
	ALBEDO = mix(tex1Color, tex2Color, mixColor).rgb;
	
	// Normal
	vec4 norm1Color = texture(Normal1, adjustedUV * TextureUVScale);
	vec4 norm2Color = texture(Normal2, adjustedUV * TextureUVScale);
	
	// Apparently this needs to be NORMAL_MAP not NORMAL, or it'll break in weird and unexpected ways. I hate shader debugging.
	NORMAL_MAP = mix(norm1Color, norm2Color, mixColor * 5.0).rgb;

	// Roughness
	ROUGHNESS = texture(Roughness, adjustedUV * TextureUVScale).r * 2.0;
	
	// Distance fade
	//float depth = texture(DepthTexture, SCREEN_UV).r;
	//depth = depth * 2.0 - 1.9;
	//ALBEDO = vec3(depth,depth,depth);
}

// thank you godotshaders.com, very cool.
vec2 rotate(vec2 uv) {
	mat2 rotation = mat2(vec2(sin(0), -cos(0)),
						vec2(cos(0), sin(0)));
	
	uv = uv * rotation;
	return uv;
}

void vertex() {
	float adjustmentAmount = Offset / SurfaceSize;
	vec2 adjustedUV = vec2(UV.x + adjustmentAmount, UV.y);
	
	VERTEX.y += texture(TerrainTexture, adjustedUV).r * texture(TerrainIntensityTexture, rotate(adjustedUV)).r * TerrainIntensity;
}