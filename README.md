# Midnight Rail

![](Assets/AppResources/splash.png)

A spooky train game written in Godot for the Academy of Interactive Entertainment

### How to play

WASD, Typical first person controls, you know the drill.

Move between carriages in a cramped train as Very Scary Monsters™ creep up to the windows. Shine your light at them to scare them off, and don't let them get in!

### FAQ.

**How do I run this?**

If you just want to play the game, head over to Itch.io and [grab the download from there](https://squidink7.itch.io/midnight-rail)

If you want to build the game from source/mess with the code, download or `git clone` this repository somewhere, then use [Godot 4.3+](https://godotengine.org/) with a recent version of [.NET](https://dotnet.microsoft.com/en-us/) (tested with 8, but in theory 6-7 should work too)

**Who made the artwork?**

The credits of all the assets used are visible in the game from the "Credits" option in the main menu.

**Who made the code?**

Yours truly~

**This game sucks**

I'm sorry to hear that 💔, if theres anything particular I can do to fix it please report an issue

**This game doesn't suck it's actually alright**

Thank you! that makes me happy :D