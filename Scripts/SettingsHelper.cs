using System;
using Godot;

static class SettingsHelper
{
	public static void SettingsUpdated()
	{
		if (Settings.Get<bool>("Fullscreen", false))
			DisplayServer.WindowSetMode(DisplayServer.WindowMode.Fullscreen);
		else
			DisplayServer.WindowSetMode(DisplayServer.WindowMode.Windowed);
	}
}