using System.Collections.Generic;
using Godot;

class FileHelper
{
	// C#'s file handling APIs don't work when the game is exported, as all the files are contained in a virtual filesystem.
	// We need to use Godot's functions for this, which frankly suck ass so heres a helper function to read all the lines for a given file.
	public static string[] ReadFile(string path)
	{
		var f = FileAccess.Open(path, FileAccess.ModeFlags.Read);
		List<string> data = new();

		while (!f.EofReached())
		{
			data.Add(f.GetLine());
		}
		f.Close();

		return data.ToArray();
	}
}