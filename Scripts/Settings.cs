using System;
using Godot;

static class Settings
{
	static string ConfigFileName = "settings.cfg";

	static ConfigFile ConfigManager = new();

	static bool HasLoaded;
	
	public static T Get<[MustBeVariant] T>(string key, Variant fallback, bool isStat = false)
	{
		Load();
		
		if (isStat)
			return ConfigManager.GetValue("Stats", key, fallback).As<T>();
		else
			return ConfigManager.GetValue("Settings", key, fallback).As<T>();
	}

	public static void Set(string key, Variant value, bool isStat = false)
	{
		Load();

		if (isStat)
			ConfigManager.SetValue("Stats", key, value);
		else
			ConfigManager.SetValue("Settings", key, value);
	}
	
	public static void Load()
	{
		if (!HasLoaded)
		{
			ConfigManager.Load($"user://{ConfigFileName}");
			HasLoaded = true;
			StatsManager.LoadStats();
		}
	}

	public static void Save()
	{
		ConfigManager.Save($"user://{ConfigFileName}");
	}
}