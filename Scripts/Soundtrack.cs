using Godot;
using System;

public partial class Soundtrack : AudioStreamPlayer
{
	public override void _Ready()
	{
		Play(33.5f);
		var volumeTween = GetTree().CreateTween();
		volumeTween.TweenProperty(this, "volume_db", -10, 40);
	}
}
