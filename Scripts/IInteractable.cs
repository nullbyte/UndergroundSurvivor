using System;

public interface IInteractable
{
	public void Activated(Player player);
}
