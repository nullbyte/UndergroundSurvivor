using System.Collections.Generic;
using Godot;

static class StatsManager
{
	public static Dictionary<string, int> Stats = new()
	{
		{"NewGames", 0},
		{"Deaths", 0},
		{"MonstersDefeated", 0},
		{"GamesWon", 0},
	};

	public static void LoadStats()
	{
		foreach (var item in Stats.Keys)
		{
			Stats[item] = Settings.Get<int>(item, 0, true);
		}
	}
	
	public static void UpdateStats()
	{
		foreach (var item in Stats)
		{
			Settings.Set(item.Key, item.Value, true);
			Settings.Save();
		}
		
		LoadStats();
	}
}