using Godot;
using System;

public partial class HintProvider: Node {
	Game? Game;
	GameProgression? GameProgression;

	bool HasMoved = false;
	bool HasUsedDoor = false;
	bool HasCharged = false;
	bool HasUsedFlashLight = false;
	
	public override void _Ready()
	{
		Game = GetNode<Game>("/root/Game");
		GameProgression = GetNodeOrNull<GameProgression>("/root/Game/GameProgression");
		
		if (GameProgression != null)
			GameProgression.GameEvent += (id) => 
			{
				if ((GameProgression.Events)id == GameProgression.Events.TUTORIAL)
				{
					RunTutorial();
				}
			};
	}

	public async void RunTutorial()
	{
		
		// Hint completion conditions
		if (GameProgression != null)
			GameProgression.GameEvent += (id) => {
				if (id != (int)GameProgression.Events.TUTORIAL)
				{
					HasMoved = true;
					HasUsedDoor = true;
					HasCharged = true;
					Game!.CompleteHint();
				}
			};
		
		Game!.DoorActivated += () => {HasUsedDoor = true; Game.CompleteHint("Walk through the carriage door");};
		Game.ChargerActivated += OnChargerActivated;
		
		
		// Tutorial

		// Movement
		await ToSignal(GetTree().CreateTimer(2), "timeout");
		if (!HasMoved)
		{
			await Game.ShowHint("Use WASD to move");
		}

		// Door
		await ToSignal(GetTree().CreateTimer(3), "timeout");
		if (!HasUsedDoor)
		{
			await Game.ShowHint("Walk through the carriage door");
		}
		
		// Charging
		await ToSignal(GetTree().CreateTimer(2), "timeout");
		if (!HasCharged)
		{
			await Game.ShowHint("Press E over a blue charger to recharge your light");
		}

		// Charging
		await ToSignal(GetTree().CreateTimer(2), "timeout");
		if (!HasUsedFlashLight)
		{
			await Game.ShowHint("Press F to toggle your flashlight");
		}
	}

	void OnChargerActivated()
	{
		if (!HasCharged)
		{
			HasCharged = true;
			Game!.CompleteHint("Press E over a blue charger to recharge your light");
		}
	}

	public void OnCrouchPrompt(bool show)
	{
		if (show)
			_=Game!.ShowHint("Press Ctrl to crouch");
		else
			Game!.CompleteHint("Press Ctrl to crouch");
	}
	
	public override void _Input(InputEvent ev)
	{
		if (!HasMoved) {
			if (ev.IsActionPressed("MoveForward") || ev.IsActionPressed("MoveBackward") || ev.IsActionPressed("MoveLeft") || ev.IsActionPressed("MoveRight")) {
				HasMoved = true; Game!.CompleteHint("Use WASD to move");
			}
		}

		if (!HasUsedFlashLight) {
			if (ev.IsActionPressed("FlashLightToggle")) {
				HasUsedFlashLight = true; Game!.CompleteHint("Press F to toggle your flashlight");
			}
		}
	}
}
