using Godot;
using System;

public partial class ScreenshotCamera : Camera3D
{
	[Export] bool Enabled = false;
	// Called when the node enters the scene tree for the first time.
	public override async void _Ready()
	{
		if (Enabled) {			
			var viewport = GetParent<SubViewport>();
			viewport.RenderTargetUpdateMode = SubViewport.UpdateMode.Once;
			await ToSignal(GetTree(), "process_frame");
			await ToSignal(RenderingServer.Singleton, "frame_post_draw");
			viewport.GetTexture().GetImage().SavePng("user://screenshot.png");
		}
	}
}
